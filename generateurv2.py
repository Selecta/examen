# -*- coding: utf-8 -*-
"""
Created on Tue Oct 17 17:08:50 2017

@author : BOUCHET Séverin
@author : LAURENT Pierre

Updated on Tue Nov 22 16:00:00 2017
"""

def main():
    """Principal function."""
    # Importation of modules
    import psycopg2
    import argparse
    import logging
    import sys
    import random

    #initialisation logs
    from logging.handlers import RotatingFileHandler

    # create logger
    logger = logging.getLogger()

    # create template for log
    formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
    # create handler and append file with 1mb of backup
    file_handler = RotatingFileHandler('activity.log', 'a', 1000000, 1)
    # add handler to logger
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    # create another handler to write in the console
    stream_handler = logging.StreamHandler()
    logger.addHandler(stream_handler)

    # args for connexion to database
    connect_str = "dbname='radio_libre' user='s.bouchet' host='postgresql.bts-malraux72.net' password='P@ssword'"
    # use our connection values to establish a connection
    try:
        # create a psycopg2 cursor that can execute queries
        conn = psycopg2.connect(connect_str)
        cursor = conn.cursor()
    except Exception:

        logger.critical('Unable to connect to the database')
        sys.exit(0)

    # create arguments and description of this script
    parser = argparse.ArgumentParser(description='Générateur de playlist')
    # create argument for the duration of the playlist
    parser.add_argument('-d', '--duree', help='Duration of playlist', required=True)

    # Log level dictionnary to help defining level inside CLI
    logleveldict = {'DEBUG' : logging.DEBUG,
                    'INFO' : logging.INFO,
                    'WARNING' : logging.WARNING,
                    'ERROR' : logging.ERROR,
                    'CRITICAL' : logging.CRITICAL}
    parser.add_argument('-l', '--log',
                        help='Set log level to [DEBUG], INFO, WARNING, ERROR or CRITICAL',
                        metavar='LEVEL',
                        choices=logleveldict,
                        default='DEBUG')

    # create argument for the genre of tracks and percentage
    # parser.add_argument('-g','--genre', help='Genre of tracks', required=True)
    def mytuple(parametres):
        """Function for percentage."""

        if ',' in parametres and isinstance(parametres, str):
            param, quantite = parametres.split(sep=',', maxsplit=1)
            try:
                quantite = int(quantite)
            except:
                raise argparse.ArgumentTypeError("Values must be : str,[int]")
            return param, quantite
        else:
            return parametres, 100

    parser.add_argument('-g', '--genre', metavar='str,[int]',
                        help="Genre of playlist, [and percentage of this kind]",
                        type=mytuple, action='append')
    parser.add_argument('--sous_genre', metavar='str,[int]',
                        help='Trie par sous-genre de morceaux dans la playlist',
                        type=mytuple, action='append')
    parser.add_argument('-A', '--album', metavar='str,[int]',
                        help='Tri par nom d\'album de la playlist',
                        type=mytuple, action='append')
    parser.add_argument('-t', '--titre', metavar='str,[int]',
                        help='Tri par titre de morceaux dans la playlist',
                        type=mytuple, action='append')
    parser.add_argument('-a', '--artiste', metavar='str,[int]',
                        help="Artist of playlist, [and percentage of this kind]",
                        type=mytuple, action='append')

    # create argument for the name of the playlist
    parser.add_argument('name', help='Name of the file who is created (Format : name.txt)')

    args = parser.parse_args()

    # Set log level based on CLI user parameter
    logger.setLevel(logleveldict[args.log])
    file_handler.setLevel(logleveldict[args.log])
    stream_handler.setLevel(logleveldict[args.log])
    output = []

    if args.genre is not None or args.artiste is not None or args.album is not None or args.titre is not None:
        pourcentagetotal = 0

        for critere in ['genre', 'artiste', 'titre', 'album']:
            listelocale = []
            sommeparametre = 0
            if getattr(args, critere) is not None:
                for element in getattr(args, critere):
                    qtecritere = element[1]
                    pourcentagetotal = pourcentagetotal + qtecritere
                    sommedurees = 0
                    dureetotale = (int(args.duree) * qtecritere) / 100 * 60
                    cursor.execute("SELECT * FROM morceaux WHERE " +
                                   critere + " ~ '%s' ORDER BY RANDOM()" % (element[0]))
                    listelocale = cursor.fetchall()

                    cpt = 0
                    while listelocale[cpt % len(listelocale)][5] + sommedurees < dureetotale:
                        output.append(listelocale[cpt % len(listelocale)])
                        sommedurees += listelocale[cpt % len(listelocale)][5]
                        cpt = cpt + 1

                    sommeparametre += sommedurees

        if pourcentagetotal > 100:
            logger.error('The sum of the percentage of the arguments can\'t exceed 100%')
            sys.exit(0)

        if pourcentagetotal < 0:
            logger.error('The sum of the percentage of the arguments can\'t be negative')
            sys.exit(0)

        # Si la playlist contient du temps restant
        if sommedurees < dureetotale:
            # restant est le temps restant a la playlist
            restant = dureetotale - sommedurees
            cursor.execute("SELECT * FROM morceaux WHERE " +
                                   critere + " ~ '%s' AND duree < '%d' ORDER BY duree DESC" % (element[0], restant))
            req = cursor.fetchone();
            
            # si une musique compatible est trouvée et différent de nul alors
            if req is not None:
                sommedurees = sommedurees + req[5]
                output.append(req)
            logger.info('The real playlist time is :' + str(sommedurees) + "secondes")


        # print (sommedurees)

        # Random generation
        dureetotale = int(args.duree)*60-sommeparametre
        cursor.execute("SELECT * FROM morceaux ORDER BY RANDOM()")
        musiquerandom = cursor.fetchall()

        cpt = 0
        while sommedurees < dureetotale:
            output.append(musiquerandom[cpt])
            sommedurees = sommedurees + musiquerandom[cpt][5]
            cpt = cpt + 1

    else:
        listelocale = []
        cursor.execute("SELECT * FROM morceaux ORDER BY RANDOM()")
        listelocale = cursor.fetchall()
        dureetotale = int(args.duree) * 60

        cpt = 0
        while dureetotale > cpt:
            output.append(listelocale[cpt % len(listelocale)])
            cpt += listelocale[cpt % len(listelocale)][5]

    # Mix of the playlist
    random.shuffle(output)

    # Open output file
    try:
        fichier = open(args.name, "w")
    except Exception:
        logger.critical('Permission denied to open playlist file')
        sys.exit(0)


    for information in output:
        # Insert song in output file
        try:
            fichier.write(str(information[8]) + "\n")
            fichier.flush()
        except Exception:
            logger.critical('Permission denied to write playlist file')
            sys.exit(0)



if __name__ == "__main__":
    main()

